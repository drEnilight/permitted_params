defmodule PermittedParams do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      import unquote(__MODULE__), only: [params: 1]
    end
  end

  defmacro params(do: {:__block__, _meta, fields}) do
    ecto_data = transform_block(fields)

    quote do
      use unquote(__MODULE__)

      import Ecto.Changeset

      @primary_key false
      embedded_schema do
        unquote(ecto_data[:schema])
      end

      def cast(params) do
        __MODULE__
        |> struct()
        |> cast(params, unquote(ecto_data[:fields]))
        |> validate_required(unquote(ecto_data[:required]))
        |> format_params()
      end

      defp format_params(%Ecto.Changeset{valid?: false} = changeset),
        do: {:error, changeset}

      defp format_params(%Ecto.Changeset{valid?: true} = changeset) do
        params =
          changeset
          |> apply_changes()
          |> Map.from_struct()
          |> Enum.into(%{}, fn {key, value} -> {key, {Map.get(unquote(Macro.escape(ecto_data[:formats])), key), value}} end)

        {:ok, params}
      end
    end
  end

  defp transform_block(fields) do
    Enum.reduce(fields, %{fields: [], required: [], schema: [], formats: %{}}, fn {type, info, [name, opts]}, %{fields: fields, required: required, schema: schema, formats: formats} = acc ->
      acc = Map.put(acc, :fields, [name | fields])

      acc = case type do
        :requires -> Map.put(acc, :required, [name | required])
        _ -> acc
      end

      {format, type} = Keyword.get(opts, :format)

      acc
      |> Map.put(:formats, Map.put(formats, name, format))
      |> Map.put(:schema, [{:field, info, [name, type, Keyword.delete(opts, :format)]} | schema])
    end)
  end
end
