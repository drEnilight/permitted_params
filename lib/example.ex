defmodule Example do
  use PermittedParams

  params do
    requires :title, format: {:eq, :string}
    requires :is_active, format: {:eq, :boolean}
    optional :updated_at, format: {:gte, :utc_datetime}
  end


  # OR
  # params do
  #   field :title, :string, required: true, format: :ilike
  #   field :is_active, :boolean, format: :eq
  #   field :updated_at, :utc_datetime, format: :gte
  # end
end


iex(1)> Example.cast(%{"is_active" => true, "updated_at" => DateTime.utc_now(), "query" => "hello"})
{:error,
 #Ecto.Changeset<
   action: nil,
   changes: %{is_active: true, updated_at: ~U[2023-11-26 14:37:52Z]},
   errors: [title: {"can't be blank", [validation: :required]}],
   data: #Example<>,
   valid?: false
 >}
iex(2)> Example.cast(%{"title" => "hello", "is_active" => true, "updated_at" => DateTime.utc_now(), "query" => "hello"})
{:ok,
 %{
   is_active: {:eq, true},
   title: {:eq, "hello"},
   updated_at: {:gte, ~U[2023-11-26 14:37:53Z]}
 }}
